# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Security::Finding do
  describe 'associations' do
    it { is_expected.to belong_to(:scan).required }
    it { is_expected.to belong_to(:scanner).required }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:project_fingerprint) }
    it { is_expected.to validate_length_of(:project_fingerprint).is_at_most(40) }
  end

  describe '.by_project_fingerprint' do
    let!(:finding_1) { create(:security_finding) }
    let!(:finding_2) { create(:security_finding) }
    let(:expected_findings) { [finding_1] }

    subject { described_class.by_project_fingerprint(finding_1.project_fingerprint) }

    it { is_expected.to match_array(expected_findings) }
  end
end
